<?php 
class LocationsTableSeeder extends Seeder {
 
       public function run()
       {
         //delete users table records
         DB::table('locations')->delete();
         //insert some dummy records
         DB::table('locations')->insert(array(
             array('name'=>'Boston'),
             array('name'=>'New York'),
             array('name'=>'Berlin'),
             array('name'=>'Moscow'),


          ));
       }
 
}