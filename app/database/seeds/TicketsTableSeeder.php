<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class TicketsTableSeeder extends Seeder {

	public function run()

	{
		DB::table('tickets')->delete();
		$faker = Faker::create();

		foreach(range(1, 10) as $index)
		{
			Ticket::create([
				'title' => $faker->sentence(),
				'body' => $faker->realText(1000),
				'user_id' => rand(1,2),
				'status_id' => rand(1,4),
				'location_id' => rand(1,4),
				'time_spent' => rand(1,24),
			
			]);
		}
	}

}