<?php

class RolesTableSeeder extends Seeder {

    public function run()
    {
        DB::table('roles')->delete();

        $adminRole = new Role;
        $adminRole->name = 'admin';
        $adminRole->save();

        $userRole = new Role;
        $userRole->name = 'user';
        $userRole->save();

        $user1 = User::find(1);
        $user2 = User::find(2);

        
        $user1->attachRole( $adminRole );

        
        $user2->attachRole( $userRole );
    }

}
