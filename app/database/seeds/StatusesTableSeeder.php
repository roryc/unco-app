<?php 
class StatusesTableSeeder extends Seeder {
 
       public function run()
       {
         //delete users table records
         DB::table('statuses')->delete();
         //insert some dummy records
         DB::table('statuses')->insert(array(
             array('type'=>'open'),
             array('type'=>'pending'),
             array('type'=>'in progress'),
             array('type'=>'completed'),


          ));
       }
 
}