
<!DOCTYPE html>
<html lang="en">

<head>
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
   <meta name="description" content="Bootstrap Admin App + jQuery">
   <meta name="keywords" content="app, responsive, jquery, bootstrap, dashboard, admin">
   <title>UNCO Login</title>
   <!-- =============== VENDOR STYLES ===============-->
      <!-- =============== VENDOR STYLES ===============-->
   <!-- FONT AWESOME-->
   {{ HTML::style("vendor/fontawesome/css/font-awesome.min.css");}}
   <!-- SIMPLE LINE ICONS-->
   {{ HTML::style("vendor/simple-line-icons/css/simple-line-icons.css");}}
   <!-- ANIMATE.CSS-->
   {{ HTML::style("vendor/animate.css/animate.min.css");}}
   <!-- WHIRL (spinners)-->
   {{ HTML::style("vendor/whirl/dist/whirl.css");}}
   <!-- =============== PAGE VENDOR STYLES ===============-->
   <!-- =============== APP STYLES ===============-->
   {{ HTML::style("css/app.css", array("id" => "maincss"));}}
</head>

<body>
   <div class="wrapper">
      <div class="block-center mt-xl wd-xl">
         <!-- START panel-->
         <div class="panel panel-dark panel-flat">
            <div class="panel-heading text-center">
               <a href="#">
                  UNCO APP
               </a>
            </div>
            <div class="panel-body">
               <p class="text-center pv">SIGN IN TO CONTINUE.</p>
               {{ Form::open(array('route'=> 'sessions.store')) }}
               
                  <div class="form-group has-feedback">
                    {{ Form::text('email', $value = null, array('placeholder' => 'email', 'type'=>'text', 'class' => 'form-control')) }}
                     <span class="fa fa-envelope form-control-feedback text-muted"></span>
                  </div>
                  <div class="form-group has-feedback">
                    {{ Form::password('password', array('placeholder' => 'password', 'type'=>'text', 'class' => 'form-control'))}}
                     <span class="fa fa-lock form-control-feedback text-muted"></span>
                  </div>
                  <div class="clearfix">
                     <div class="checkbox c-checkbox pull-left mt0">
                        <label>
                           <input type="checkbox" value="" name="remember">
                           <span class="fa fa-check"></span>Remember Me</label>
                     </div>
                    
                  </div>
                 
                 {{ Form::submit('Sign in', array('class' => 'btn btn-block btn-primary mt-lg')) }}
			     {{ Form::close() }}
               
             
            </div>
         </div>
         <!-- END panel-->
         <div class="p-lg text-center">
            <span>&copy;</span>
            <span>2015</span>
            <span>-</span>
            <span>UNCO APP</span>
            <br>
            <span>UNCO APP</span>
         </div>
      </div>
   </div>
   <!-- =============== VENDOR SCRIPTS ===============-->
    {{ HTML::script("vendor/modernizr/modernizr.js"); }}
   <!-- JQUERY-->

   <!-- BOOTSTRAP-->
   {{ HTML::script("vendor/bootstrap/dist/js/bootstrap.js"); }}
   <!-- STORAGE API-->
   {{ HTML::script("vendor/jQuery-Storage-API/jquery.storageapi.js"); }}
   <!-- JQUERY EASING-->
   {{ HTML::script("vendor/jquery.easing/js/jquery.easing.js"); }}
   <!-- ANIMO-->
   {{ HTML::script("vendor/animo.js/animo.js"); }}
   <!-- LOCALIZE-->
   {{ HTML::script("vendor/jquery-localize-i18n/dist/jquery.localize.js"); }}
   <!-- =============== PAGE VENDOR SCRIPTS ===============-->
   <!-- =============== APP SCRIPTS ===============-->
   {{ HTML::script("js/app.js"); }}
 
</body>

</html>