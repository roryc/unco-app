<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>UNCO APP</title>
	{{ HTML::style('//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css')}}
	{{HTML::script('http://code.jquery.com/jquery-1.10.1.min.js')}}
	{{HTML::script('//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js')}}
	<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/datatables/1.10.4/css/jquery.dataTables.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.4/js/jquery.dataTables.min.js"></script>


</head>
<body>
<header>
	<div class="container">
	<h1>NAME OF COMPANY</h1>

	 <h3>{{link_to(URL::previous(), 'Back')}}</h3>
		
	</div>
</header>
<main class="container">
@yield('content')

</main>

	
</body>
</html>
