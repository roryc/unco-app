<!DOCTYPE html>
<html lang="en">

<head>
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
   <meta name="description" content="Bootstrap Admin App + jQuery">
   <meta name="keywords" content="app, responsive, jquery, bootstrap, dashboard, admin">
   <title>Unco App</title>
 

  
   {{ HTML::script('vendor/jquery/dist/jquery.js'); }}

   {{ HTML::script("https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.4/js/jquery.dataTables.min.js");}}
  {{ HTML::style("//cdnjs.cloudflare.com/ajax/libs/datatables/1.10.4/css/jquery.dataTables.min.css");}}
    





   <!-- =============== VENDOR STYLES ===============-->
   <!-- FONT AWESOME-->
   {{ HTML::style("vendor/fontawesome/css/font-awesome.min.css");}}
   <!-- SIMPLE LINE ICONS-->
   {{ HTML::style("vendor/simple-line-icons/css/simple-line-icons.css");}}
   <!-- ANIMATE.CSS-->
   {{ HTML::style("vendor/animate.css/animate.min.css");}}
   <!-- WHIRL (spinners)-->
   {{ HTML::style("vendor/whirl/dist/whirl.css");}}
   <!-- =============== PAGE VENDOR STYLES ===============-->
   <!-- =============== APP STYLES ===============-->
   {{ HTML::style("css/app.css", array("id" => "maincss"));}}


 {{ HTML::script("vendor/jquery-localize-i18n/dist/jquery.localize.js"); }}
</head>

<body>


   <div class="wrapper">
      <!-- top navbar-->
      <header class="topnavbar-wrapper">
         <!-- START Top Navbar-->
         <nav role="navigation" class="navbar topnavbar">
            <!-- START navbar header-->
            <div class="navbar-header">
               <a href="#/" class="navbar-brand">
                  <div class="brand-logo">
                    
                     
                     {{ HTML::image('img/logo.png', 'logo', array('class' => 'img-responsive')) }}
                  </div>
                  <div class="brand-logo-collapsed">
           
                     
                         {{ HTML::image('img/logo-single.png', 'logo', array('class' => 'img-responsive')) }}
                  </div>
               </a>
            </div>
            <!-- END navbar header-->
            <!-- START Nav wrapper-->
            <div class="nav-wrapper">
               <!-- START Left navbar-->
               <ul class="nav navbar-nav">
                  <li>
                     <!-- Button used to collapse the left sidebar. Only visible on tablet and desktops-->
                     <a href="#" data-toggle-state="aside-collapsed" class="hidden-xs">
                        <em class="fa fa-navicon"></em>
                     </a>
                     <!-- Button to show/hide the sidebar on mobile. Visible on mobile only.-->
                     <a href="#" data-toggle-state="aside-toggled" data-no-persist="true" class="visible-xs sidebar-toggle">
                        <em class="fa fa-navicon"></em>
                     </a>
                  </li>
               </ul>
               <!-- END Left navbar-->
               <!-- START Right Navbar-->
               <ul class="nav navbar-nav navbar-right">
                  <!-- Search icon-->
                  <li>
                     <a href="#" data-search-open="">
                        <em class="icon-magnifier"></em>
                     </a>
                  </li>
                  <!-- START Offsidebar button-->
                  <li>
                     <a href="#" data-toggle-state="offsidebar-open" data-no-persist="true">
                        <em class="icon-notebook"></em>
                     </a>
                  </li>
                  <!-- END Offsidebar menu-->
               </ul>
               <!-- END Right Navbar-->
            </div>
            <!-- END Nav wrapper-->
            <!-- START Search form-->

            <form role="search" class="navbar-form">
               <div class="form-group has-feedback">
                  <input type="text" id="search-box" placeholder="Type and hit enter ..." class="form-control">
                  <div data-search-dismiss="" class="fa fa-times form-control-feedback"></div>
               </div>
               <button type="submit" class="hidden btn btn-default">Submit</button>
            </form>
            <!-- END Search form-->
         </nav>
         <!-- END Top Navbar-->
      </header>
      <!-- sidebar-->
      <aside class="aside">
         <!-- START Sidebar (left)-->
         <div class="aside-inner">
            <nav class="sidebar">
               <!-- START sidebar nav-->
               <ul class="nav">
                  <!-- Iterates over all sidebar items-->
                  <li class="nav-heading ">
                     <span data-localize="sidebar.heading.HEADER">Menu Navigation</span>
                  </li>
                  <li {{ Request::is('*alltickets') ? 'class="active"' : '' }}>
                     <a href="{{ URL::route('all.tickets')}}" title="Tickets">
                        <em class="fa fa-tasks"></em>
                        <span data-localize="sidebar.Tickets">Tickets</span>
                     </a>
                  @if (Entrust::hasRole('admin') )
                  </li>
                     <li {{ Request::is('*users') ? 'class="active"' : '' }}>
                     <a href="{{ URL::route('users')}}" title="Users">
                        <em class="fa fa-user"></em>
                        <span data-localize="sidebar.Users">Users</span>
                     </a>
                 
                  </li>
                    <li {{ Request::is('*locations') ? 'class="active"' : '' }}>
                     <a href="{{ URL::route('locations')}}" title="Locations">
                        <em class="fa fa-building"></em>
                        <span data-localize="sidebar.Locations">Locations</span>
                     </a>
                 
                  </li>
                  @endif

                    <li {{ Request::is('*logout') ? 'class="active"' : '' }}>
                     <a href="{{ URL::route('logout')}}" title="Logout">
                        <em class="fa fa-sign-out"></em>
                        <span data-localize="sidebar.Logout">Logout</span>
                     </a>
                 
                  </li>
                  <li class=" ">
                     <a href="#menuid" title="Menu" data-toggle="collapse">
                        <em class="icon-folder"></em>
                        <span data-localize="sidebar.nav.menu.MENU">Menu</span>
                     </a>
                     <ul id="menuid" class="nav sidebar-subnav collapse">
                        <li class="sidebar-subnav-header">Menu</li>
                        <li class=" ">
                           <a href="submenu.html" title="Sub Menu">
                              <em></em>
                              <span data-localize="sidebar.nav.menu.SUBMENU">Sub Menu</span>

                           </a>
                        </li>
                              <li class="sidebar-subnav-header">Menu</li>
                        <li class=" ">
                           <a href="submenu.html" title="Sub Menu">
                              <em></em>
                              <span data-localize="sidebar.nav.menu.SUBMENU">Sub Menu</span>
                              
                           </a>
                        </li>

                        
                     </ul>
                  </li>
               </ul>
               <!-- END sidebar nav-->
            </nav>
         </div>
         <!-- END Sidebar (left)-->
      </aside>
      <!-- offsidebar-->
      <aside class="offsidebar">
         <!-- START Off Sidebar (right)-->
         <nav>
            <div role="tabpanel">
               <!-- Nav tabs-->
               <ul role="tablist" class="nav nav-tabs nav-justified">
                  <li role="presentation" class="active">
                     <a href="#app-settings" aria-controls="app-settings" role="tab" data-toggle="tab">
                        <em class="icon-equalizer fa-lg"></em>
                     </a>
                  </li>
                  <li role="presentation">
                     <a href="#app-chat" aria-controls="app-chat" role="tab" data-toggle="tab">
                        <em class="icon-users fa-lg"></em>
                     </a>
                  </li>
               </ul>
               <!-- Tab panes-->
               <div class="tab-content">
                  <div id="app-settings" role="tabpanel" class="tab-pane fade in active">
                     <h3 class="text-center text-thin">Tab 1</h3>
                  </div>
                  <div id="app-chat" role="tabpanel" class="tab-pane fade">
                     <h3 class="text-center text-thin">Tab 2</h3>
                  </div>
               </div>
            </div>
         </nav>
         <!-- END Off Sidebar (right)-->
      </aside>
      <!-- Main section-->
      <section>

         <!-- Page content-->
         <div class="content-wrapper">
            <div class="content-heading">
               <!-- START Language list-->
               <div class="pull-right">
                  <div class="btn-group">
                     <button type="button" data-toggle="dropdown" class="btn btn-default">English</button>
                     <ul role="menu" class="dropdown-menu dropdown-menu-right animated fadeInUpShort">
                        <li><a href="#" data-set-lang="en">English</a>
                        </li>
                        <li><a href="#" data-set-lang="es">Spanish</a>
                        </li>
                         <li><a href="#" data-set-lang="fi">Finish</a>
                        </li>
                        <li><a href="#" data-set-lang="de">German</a>
                        </li>

                     </ul>
                  </div>
               </div>
               <!-- END Language list    -->
              {{ Auth::user()->name}}
               <small data-localize="dashboard.WELCOME"></small>
            </div>
            <div class="row">
               <div class="col-xs-12 text-center">
               
              
@yield('content')

                  
               </div>
            </div>
         </div>
      </section>


      <!-- Page footer-->
      <footer>
         <span data-localize="dashboard.WELCOME"></span>
      </footer>

   </div>
   <!-- =============== VENDOR SCRIPTS ===============-->
    {{ HTML::script("vendor/modernizr/modernizr.js"); }}
   <!-- JQUERY-->

   <!-- BOOTSTRAP-->
   {{ HTML::script("vendor/bootstrap/dist/js/bootstrap.js"); }}
   <!-- STORAGE API-->
   {{ HTML::script("vendor/jQuery-Storage-API/jquery.storageapi.js"); }}
   <!-- JQUERY EASING-->
   {{ HTML::script("vendor/jquery.easing/js/jquery.easing.js"); }}
   <!-- ANIMO-->
   {{ HTML::script("vendor/animo.js/animo.js"); }}
   <!-- LOCALIZE-->
 
   <!-- =============== PAGE VENDOR SCRIPTS ===============-->
   <!-- =============== APP SCRIPTS ===============-->
   {{ HTML::script("js/app.js"); }}
 
 

</body>

</html>