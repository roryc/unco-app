@extends('_layouts.index')
@section('content')
 
<div class="row">
  <div class="col-md-12">
  <h1 data-localize="sidebar.Tickets">All Tickets</h1>
  

  <h2>{{ link_to_route('tickets.create', 'New Job', array(), array("data-localize" => "main.NewJob"))}}</h2>
  {{ $table->render() }}
  {{ $table->script() }}
  </div>
</div>
@stop
