@extends('_layouts.index')


@section('content')
<h1 data-localize = main.NewUser>Create a new user</h1>

{{ Form::open(array('route' => 'users.store'))}}

                      <fieldset>
		<div class="form-group">
		<label data-localize = "main.Name" class="col-sm-2 control-label">Name</label>
         <div class="col-sm-10">

		
	{{ Form::text('name', $value = null, array('data-localize' => 'main.Name', 'placeholder' => 'name', 'type'=>'text', 'class' => 'form-control'))}}
		
  </div>
                        </div>
                     </fieldset>
	        <fieldset>
		<div class="form-group">
		<label data-localize = "main.Email" class="col-sm-2 control-label">Email</label>
         <div class="col-sm-10">

		
{{ Form::text('email', $value = null, array('data-localize' => 'main.Email', 'placeholder' => 'email', 'type'=>'text', 'class' => 'form-control'))}}
		
  </div>
                        </div>
                     </fieldset>
                     	        <fieldset>
		<div class="form-group">
		<label data-localize = "main.Password" class="col-sm-2 control-label">Password</label>
         <div class="col-sm-10">

		
{{ Form::password('password', array('data-localize' => 'main.Password', 'placeholder' => 'password', 'type'=>'text', 'class' => 'form-control'))}}
		
  </div>
                        </div>
                     </fieldset>

                     <fieldset>  
   <div class="form-group">
                           <label data-localize = "main.Role" class="col-sm-2 control-label">Role</label>
                           <div class="col-sm-10">
		{{ Form::select('role', Role::lists('name', 'id')) }}
		
		</div>
                        </div>
                     </fieldset>
                     
                     <fieldset> 


                                             <div class="form-group">
                           <div class="col-sm-12">
                             
                              {{Form::submit('Save Changes', array('data-localize' => 'main.Save', "class" => "btn btn-primary")) }} 
                           </div>
                        </div>
                     </fieldset>



{{ Form::close() }}
@stop
