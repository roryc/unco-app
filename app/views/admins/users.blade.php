@extends('_layouts.index')
@section('content')


<div class="row">
  <div class="col-md-12">
  <h1 data-localize="sidebar.Users">Users</h1>
  <h2>{{ link_to_route('users.create', 'New User', array(), array('data-localize' => 'main.NewUser'))}}</h2>
 {{ $table->render() }}
  {{ $table->script() }}
  
  </div>
</div>
@stop