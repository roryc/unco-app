@extends('_layouts.index')

@section('content')
<h1 data-localize = "main.Createlocation">Create a new location</h1>
{{ Form::open(array('route' => 'locations.store'))}}

		<div class="form-group">
		<label data-localize = "main.Nameoflocation" class="col-sm-2 control-label">Name of Location</label>
         <div class="col-sm-10">

		
		{{ Form::text('name') }}
		
		
  </div>
                        </div>
                     </fieldset>
                     <br>
                     <br>
            <fieldset> 
<div class="form-group">

                             
{{Form::submit('Save Changes', array('data-localize' => 'main.Save', "class" => "btn btn-primary")) }} 
                           </div>
                        </div>
                     </fieldset>


{{ Form::close() }}
@stop

