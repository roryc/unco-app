@extends('_layouts.index')
@section('content')

<div class="row">
  <div class="col-md-12">
  <h1 data-localize="sidebar.Locations">All Locations</h1>

    <h2>{{ link_to_route('locations.create', 'New Location', array(), array('data-localize' => 'main.NewLocation'))}}</h2>
  {{ $table->render() }}
  {{ $table->script() }}
  </div>
</div>
@stop
