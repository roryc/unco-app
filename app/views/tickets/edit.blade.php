@extends('_layouts.index')

@section('content')
<h1> Edit Post</h1>
{{ Form::model($ticket, array('route' => array('tickets.update', $ticket->id), 'method' => 'put'))}}

<fieldset>
                        <div class="form-group">
                           <label class="col-sm-2 control-label">Status</label>
                           <div class="col-sm-10">
		{{ Form::select('status_id', Status::lists('type', 'id')) }}
		{{ $errors->first('status', '<p class="error">:message</p>')}}
		</div>
                        </div>
                     </fieldset>
                     <fieldset>    

	<fieldset>
                        <div class="form-group">
                           <label class="col-sm-2 control-label">User</label>
                           <div class="col-sm-10">
		
		{{ Form::select('user_id', User::lists('name', 'id')) }}
		{{ $errors->first('status', '<p class="error">:message</p>')}}
		</div>
                        </div>
                     </fieldset>
                     <fieldset>  
   <div class="form-group">
                           <label class="col-sm-2 control-label">Location</label>
                           <div class="col-sm-10">
		{{ Form::select('location_id', Location::lists('name', 'id')) }}
		{{ $errors->first('status', '<p class="error">:message</p>')}}
		</div>
                        </div>
                     </fieldset>
                      <fieldset>
		<div class="form-group">
		<label class="col-sm-2 control-label">Title</label>
         <div class="col-sm-10">

		
		{{ Form::text('title') }}
		{{ $errors->first('title', '<p class="error">:message</p>')}}
		
  </div>
                        </div>
                     </fieldset>

                


			<div class="form-group">
		<label class="col-sm-2 control-label">Body</label>
         <div class="col-sm-10">
		
		{{ Form::textarea('body') }}
		{{ $errors->first('body', '<p class="error">:message</p>')}}

	
  </div>
                        </div>
                     </fieldset>
                     <fieldset> 


                                             <div class="form-group">
                           <div class="col-sm-12">
                             
                              {{Form::submit('Save Changes', array( "class" => "btn btn-primary")) }} 
                           </div>
                        </div>
                     </fieldset>



{{ Form::close() }}
@stop
