@extends('_layouts.index')

@section('content')
<h1 data-localize = "main.Create">Create</h1>
{{ Form::open(array('route' => 'tickets.store'))}}

<fieldset>
                        <div class="form-group">
                           <label data-localize = "main.Status" class="col-sm-2 control-label">Status</label>
                           <div class="col-sm-10">
		{{ Form::select('status_id', Status::lists('type', 'id')) }}
		{{ $errors->first('status', '<p class="error">:message</p>')}}
		</div>
                        </div>
                     </fieldset>
                       <fieldset>  

                                          <div class="form-group">
                           <label data-localize = "main.User" class="col-sm-2 control-label">User</label>
                           <div class="col-sm-10">
                           @foreach (User::all() as $user)
                           {{ Form::checkbox('user', $user->id)}}{{$user->name}}
       @endforeach

      </div>
                        </div>
                     </fieldset>

                     <fieldset>  
   <div class="form-group">
                           <label data-localize = "main.Location" class="col-sm-2 control-label">Location</label>
                           <div class="col-sm-10">
		{{ Form::select('location_id', Location::lists('name', 'id')) }}
		{{ $errors->first('status', '<p class="error">:message</p>')}}
		</div>
                        </div>
                     </fieldset>
                      <fieldset>
		<div class="form-group">
		<label data-localize = "main.Title" class="col-sm-2 control-label">Title</label>
         <div class="col-sm-10">

		
		{{ Form::text('title') }}
		{{ $errors->first('title', '<p class="error">:message</p>')}}
		
  </div>
                        </div>
                     </fieldset>

                


			<div class="form-group">
		<label data-localize = "main.Body" class="col-sm-2 control-label">Body</label>
         <div class="col-sm-10">
		
		{{ Form::textarea('body') }}
		{{ $errors->first('body', '<p class="error">:message</p>')}}

	
  </div>
                        </div>
                     </fieldset>
                     <fieldset> 


                                             <div class="form-group">
                           <div class="col-sm-12">
                             
                              {{Form::submit('Save Changes', array('data-localize' => 'main.Save', "class" => "btn btn-primary")) }} 
                           </div>
                        </div>
                     </fieldset>



{{ Form::close() }}
@stop
