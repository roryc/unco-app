<?php

class Ticket extends \Eloquent {

	// Add your validation rules here
	public static $rules = [
		'title' => 'required|between:3,300',
		'body' => 'required',
		'status_id' => 'required',
		'created_at' => 'integer',
		'updated_at' => 'integer',
		'time_spent' => 'integer'

	];

	// Don't forget to fill this array
	protected $fillable = ['title', 'body', 'status_id', 'created_at', 'location_id', 'updated_at', 'time_spent'];

	public function statuses(){
		return $this->hasMany('statuses');
	}

	public function user(){
		return $this->belongsToMany('User');
	}

}