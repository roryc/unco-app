<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
use Zizaco\Entrust\HasRole;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use HasRole;
	use UserTrait, RemindableTrait;

	public static $auth_rules = [
	'email' => 'required|email',
	'password' => 'required'
	]; 

	public static $rules = [
		'name' => 'required|between:3,300',
		'email' => 'required',
		'password' => 'required'
		
	];

	public function task(){
		return $this->belongsToMany('Tickets');
	}


	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');

	protected $fillable = ['name', 'email', 'password'];



}
