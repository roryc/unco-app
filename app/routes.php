<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', array('as' => 'home', function()
{
	return View::make('hello');
}));


Route::resource('sessions', 'SessionsController');

Route::get('login', array('as' => 'login', 'uses' => 'SessionsController@create'));
Route::get('logout', array('as' => 'logout', 'uses' => 'SessionsController@destroy'));

Route::get('rorytest', function()
{
	$ticket = Ticket::first();
	$user = User::find(1);
	$user2 = User::find(2);

	$ticket->user()->attach(1);
	$ticket->user()->attach(2);

	return $ticket->user;

});


Route::group(array('before' => 'auth'), function(){

	Route::resource('tickets', 'TicketsController');
	Route::get('ticket/{id}', array('as' => 'ticket', 'uses' => 'TicketsController@getTicket'))->where('id', '[1-9][0-9]*');
	Route::get('api/tickets', array('as' => 'api.tickets', 'uses' => 'TicketsController@getTicketsDataTable'));

});


Route::group(array('before' => 'admin_role'), function() {
	Route::resource('admin', 'AdminsController');
	/*Route::get('allusers',array('as' => 'allusers', 'uses' => 'AdminsController@users'));
	
	Route::get('api/users', array('as' => 'api.allusers', 'uses' => 'AdminsController@getUsersDataTable'));*/
	Route::resource('users', 'UsersController');
	Route::get('users',array('as' => 'users', 'uses' => 'UsersController@users'));
	Route::get('api/users', array('as' => 'api.users', 'uses' => 'UsersController@getUsersDataTable'));
	Route::get('alltickets', array('as' => 'all.tickets', 'uses' => 'AdminsController@tickets'));
	Route::get('api/alltickets', array('as' => 'api.alltickets', 'uses' => 'AdminsController@getTicketsDataTable'));
	Route::resource('locations', 'LocationsController');
	Route::get('locations', array('as' => 'locations', 'uses' => 'AdminsController@locations'));
	Route::get('api/locations', array('as' => 'api.locations', 'uses' => 'AdminsController@getLocationsDataTable'));
	
	
	

});




