<?php

class SessionsController extends \BaseController {


	/**
	 * Show the form for creating a new session
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('sessions.create');
	}

	/**
	 * Store a newly created session in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), User::$auth_rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		If (Auth::attempt(array('email' => Input::get('email'), 'password' => Input::get('password')))){
			return Redirect::intended('alltickets');
		}

		return Redirect::route('home');
	}


	/**
	 * Display the specified session.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$session = Session::findOrFail($id);

		return View::make('sessions.show', compact('session'));
	}



	/**
	 * Remove the specified session from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy()
	{
		Auth::logout();

		return Redirect::route('login');
	}

}
