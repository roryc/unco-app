<?php

class AdminsController extends \BaseController {

	/**
	 * Display a listing of admins
	 *
	 * @return Response
	 */
	public function index()
	{
		$tickets = Ticket::all();


		return View::make('admins.index', compact('tickets'));
	}

	/**
	 * Show the form for creating a new admin
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('admins.create');
	}

	/**
	 * Store a newly created admin in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Admin::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		Admin::create($data);

		return Redirect::route('admins.index');
	}

	/**
	 * Display the specified admin.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$admin = Admin::findOrFail($id);

		return View::make('admins.show', compact('admin'));
	}

	/**
	 * Show the form for editing the specified admin.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$admin = Admin::find($id);

		return View::make('admins.edit', compact('admin'));
	}

	/**
	 * Update the specified admin in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$admin = Admin::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Admin::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$admin->update($data);

		return Redirect::route('admins.index');
	}

	/**
	 * Remove the specified admin from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Admin::destroy($id);

		return Redirect::route('admins.index');
	}

	protected $layout = '_layouts.index';




	public function tickets(){
		$table = Datatable::table()
		->addColumn('Title', 'Status', 'User', 'Location')
		->setUrl(route('api.alltickets'))
		->noScript();
		$this->layout->content = View::make('admins.tickets', compact('tickets'), array('table' => $table));
	}


public function getTicketsDataTable(){

	

    $query = Ticket::select('title', 'id', 'status_id', 'user_id', 'location_id', 'created_at', 'created_at', 'updated_at', 'time_spent')->get();

    return Datatable::collection($query)
        ->addColumn('title', function($model){
        #    return $model->title;
	     return link_to_route('tickets.edit', $model->title, array($model->id));
	})
        ->addColumn('status', function($model){
            return Status::find($model->status_id)->type;
        })

          ->addColumn('User', function($model){
            return User::find($model->user_id)->name;
        })
 
 
     
        ->addColumn('Location', function($model){
            return Location::find($model->location_id)->name;
        })
        ->searchColumns('title')
        ->orderColumns('status', 'updated_at')
        ->make();
}

public function locations(){
		$table = Datatable::table()
		->addColumn('Name')
		->setUrl(route('api.locations'))
		->noScript();
		$this->layout->content = View::make('locations.index', compact('locations'), array('table' => $table));
	}


public function getLocationsDataTable(){

	

    $query = Location::select('id', 'name')->get();

    return Datatable::collection($query)

     
        ->addColumn('name', function($model){
            return $model->name;
        })
        ->searchColumns('name')
        
        ->make();
}



}
