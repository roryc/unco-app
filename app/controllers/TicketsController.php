<?php

class TicketsController extends \BaseController {

	/**
	 * Display a listing of tickets
	 *
	 * @return Response
	 */
	protected $layout = '_layouts.index';
	public function index(){
		$table = Datatable::table()
		->addColumn('Title', 'User', 'Status', 'Location')
		->setUrl(route('api.tickets'))
		->noScript();
		$this->layout->content = View::make('tickets.index', compact('tickets'), array('table' => $table));
	}


	public function getticket($id){
		$ticket = Ticket::all()->find($id);
		return View::make('tickets.show', compact('ticket'));
	}

	/**
	 * Show the form for creating a new ticket
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('tickets.create');
	}

	/**
	 * Store a newly created ticket in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Ticket::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		

		Ticket::create($data);
		if (isset($input['user'])) {
    // Pass the array of ids to sync method
    	$ticket->user()->sync($input['user']);
		}	

		return Redirect::route('all.tickets');
	}

	/**
	 * Display the specified ticket.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$ticket = Ticket::findOrFail($id);

		return View::make('tickets.show', compact('ticket'));
	}

	/**
	 * Show the form for editing the specified ticket.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$ticket = Ticket::find($id);

		return View::make('tickets.edit', compact('ticket'));
	}

	/**
	 * Update the specified ticket in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$ticket = Ticket::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Ticket::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$ticket->update($data);

		return Redirect::route('all.tickets');
	}

	/**
	 * Remove the specified ticket from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Ticket::destroy($id);

		return Redirect::route('tickets.index');
	}

public function getTicketsDataTable(){

	$id = Auth::id();




   

    return Datatable::collection(Ticket::all())
    
        ->addColumn('User', function($model){
            return $model->user;
        })

    
 
     
        ->addColumn('Location', function($model){
            return Location::find($model->location_id)->name;
        })
        ->searchColumns('title')
        ->orderColumns('status', 'updated_at')
        ->make();
}

}
