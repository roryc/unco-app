<?php

class UsersController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /users
	 *
	 * @return Response
	 */
	public function index(){
	}
		protected $layout = '_layouts.index';
	public function users(){

    $table = Datatable::table()
      ->addColumn('Name', 'Last Login', 'email')
      ->setUrl(route('api.users'))
      ->noScript();

    $this->layout->content = View::make('admins.users', array('table' => $table));
}


public function getUsersDataTable(){

    $query = User::select('name', 'updated_at', 'email')->get();

    return Datatable::collection($query)
        ->addColumn('name', function($model){
            return $model->name;
        })
        ->addColumn('updated_at', function($model){
            return date('M j, Y h:i A', strtotime($model->updated_at));
        })
        ->addColumn('email', function($model){
            return  $model->email;
        })
        ->searchColumns('name', 'updated_at')
        ->orderColumns('name', 'updated_at')
        ->make();
}

	/**
	 * Show the form for creating a new resource.
	 * GET /users/create
	 *
	 * @return Response
	 */
	public function create()
	{
		$roles =Role::all();
		return View::make('admins.newuser', compact('roles'));

		
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /users
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), User::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}
		$user = new User;
		$user->name = Input::get( 'name' );
		$user->email = Input::get( 'email' );
		$password = Input::get( 'password' );
		$user->password = Hash::make($password);
		
		if ( $user->save() )
		{
			$user->attachRole( Input::get( 'role' ) );
			return Redirect::action('UsersController@index');
		}

		

		
	}


	/**
	 * Display the specified resource.
	 * GET /users/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /users/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /users/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /users/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}